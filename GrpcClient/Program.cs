﻿using Grpc.Net.Client;
using GrpcServer;
using System;
using System.Threading.Tasks;

namespace GrpcClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Greeter.GreeterClient(channel);

            Console.WriteLine("Please write your name: ");
            var input = new HelloRequest { Name = Console.ReadLine() };
            var reply = await client.SayHelloAsync(input);

            Console.WriteLine();
            Console.ReadLine();
        }
    }
}
